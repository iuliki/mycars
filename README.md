#myCars

COMANDA GIT: Să se creeze un repository GIT nou local

git init

COMANDA GIT: Să se pună fișierul README.md în repository local 

git add .

git status

COMANDA GIT: Să se pună repository local în repository remote pe serverul GitLab 

git commit -m "commit 2"

git remote add origin git@gitlab.com:iuliki/mycars.git


COMANDA GIT: Să se pună fișiere corsa.md si astra.md în repository local 

git add .

git status

COMANDA GIT: Să se pună modificările din repository local în repository remote

git commit -m "astra and corsa"

git push


COMANDA GIT: Să se pună modificarile la fișierul README.md în repository local

git add .

git status

git commit -m "readmefile Add"


COMANDA GIT: Să se pună modificările din repository local în repository remote

git push


COMANDA GIT: Să se obțină în repository local modificările făcute în repository remote

git pull
